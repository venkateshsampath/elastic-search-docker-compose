// git clone https://bitbucket.org/venkateshsampath/elastic-search-docker-compose.git

pipeline {
    agent {
        label 'elastic-jenkins-agent'
    }

    environment {
    	DOCKER_REGISTRY = "registry.hub.docker.com"
    	BUILD_NO = "${env.BUILD_NUMBER}"
    }
 
    options {
        skipDefaultCheckout(true)
    }
 
    stages {
        stage('Git') {
            steps {
                echo '> Checking out the Git version control ...' 
                git branch: 'master', url: 'https://bitbucket.org/venkateshsampath/elastic-search-docker-compose.git'
            }
        }
        stage('Build') {
            steps {
                echo '> Building the docker images ...'
                sh 'docker version'
                sh 'docker-compose --version'
                sh 'make -s build'
            }
        }
        stage('Test') {
            steps {
                echo '> Testing the docker containers ...'
                // sh 'make -s test'
            }
        }
        stage('Publish') {
            steps {
                echo '> Publishing the docker images ...'
                withCredentials([usernamePassword(credentialsId: 'docker-registry-credentials', usernameVariable: 'USERNAME', passwordVariable: 'USERPASS')]) {
	                script {
		                sh "docker login -u=$USERNAME -p=$USERPASS https://${DOCKER_REGISTRY}"
                        sh 'make -s push'
	                }
                }
            }
        }
        stage('Deploy') {
            steps {
                input "Proceed with deployment on DEV environment?"
                milestone(1)

				echo '> Deploying the stack ...'
                sh 'sshpass -V'
				sh "rm -f /tmp/.env /tmp/docker-*.yml"
				sh "cp .env /tmp/ && echo '\nBUILD_NO=${env.BUILD_NUMBER}\n' >> /tmp/.env && cat /tmp/.env"
                sh "mkdir -p /tmp/nginx/config/ssl/ && cp -R ./nginx/config/ssl/ /tmp/nginx/config/ssl/"
                sh "cp docker-stack.yml /tmp/ && ls -altr /tmp/ | tail -4"

				withCredentials([usernamePassword(credentialsId: 'elasticsearch-dev-node', usernameVariable: 'DEPLOY_USERNAME', passwordVariable: 'DEPLOY_USERPASS')]) {
					script {
						// stop the currently running elk stack and clean up the old docker container
						try {
							sh "sshpass -p '${DEPLOY_USERPASS}' ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DEPLOY_USERNAME}@$DEV_DEPLOYMENT_HOST \"cd /tmp && docker-compose -f docker-stack.yml down --rmi=all\""
							sh "sshpass -p '${DEPLOY_USERPASS}' ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DEPLOY_USERNAME}@$DEV_DEPLOYMENT_HOST \"docker system prune --force\""
						} catch(err) {
							echo: 'caught the error while bringing down old installation: $err'
						}
						// cleanup the old .env and docker-stack.yml files
						try {
							sh "sshpass -p '${DEPLOY_USERPASS}' ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DEPLOY_USERNAME}@$DEV_DEPLOYMENT_HOST \"rm -f /tmp/.env /tmp/docker-*.yml\""
						} catch(err) {
							echo: 'caught the error while cleaning up old files: $err'
						}
						//copy the latest .env and docker-stack.yml files and bring up the elk stack
						sh "sshpass -p '${DEPLOY_USERPASS}' scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null /tmp/.env /tmp/docker-stack.yml ${DEPLOY_USERNAME}@$DEV_DEPLOYMENT_HOST:/tmp/"
						sh "sshpass -p '${DEPLOY_USERPASS}' ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DEPLOY_USERNAME}@$DEV_DEPLOYMENT_HOST \"cd /tmp && docker-compose -f docker-stack.yml up -d\""
						sh "sshpass -p '${DEPLOY_USERPASS}' ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DEPLOY_USERNAME}@$DEV_DEPLOYMENT_HOST \"cd /tmp && docker-compose -f docker-stack.yml ps\""
					}
				}
				echo '> Deployment Done!'
            }
        }
    }

    post {
        cleanup {
            echo '> Destroying the docker artifacts ...'
            sh 'make -s destroy'
        }
    }
}
