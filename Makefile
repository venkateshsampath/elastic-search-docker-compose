build:
	@docker-compose up -d --build

test:
	@sh scripts/run-tests.sh

push:
	@docker-compose push

destroy:
	@docker-compose down --rmi=all
	@docker-compose down -v
	@docker system prune --force

all:
	@make -s build test destroy

config:
	@docker-compose config