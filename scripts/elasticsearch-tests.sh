#!/usr/bin/env bash

set -euf -o pipefail
source ../.env

echo '> Running tests on elasticsearch ...'

elasticsearch_host_port=$(docker port ${COMPOSE_PROJECT_NAME}_elasticsearch_1 | grep 9200 | cut -f 2 -d ":")

sh ./wait-for-it.sh localhost:$elasticsearch_host_port --timeout=90

echo -e '\n> Creating a test index ...'

index_resp=$(curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XPUT "http://localhost:$elasticsearch_host_port/test-index" | jq '.acknowledged')

if [[ $index_resp == "true" ]];
then
    echo -e '> Index created successfully.'
else
    echo -e '> Failed to create index.'
    exit 1
fi

echo -e '\n> Adding a document to test index ...'

doc_resp=$(curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XPOST "http://localhost:$elasticsearch_host_port/test-index/_doc" -H 'Content-Type: application/json' -d'{  "message": "hello-world"}' | jq '.result')

if [[ $doc_resp == '"created"' ]];
then
    echo -e '> Document added successfully.'
else
    echo -e '> Failed to add document.'
    exit 1
fi

echo -e '\n> Checking the document count in test index ...'

curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$elasticsearch_host_port/test-index/_refresh" > /dev/null

doc_count=$(curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$elasticsearch_host_port/test-index/_count" | jq -r '.count')

if [[ $doc_count == 1 ]];
then
    echo -e '> Document count is' $doc_count;
else
    echo -e "> Wrong document count."
    exit 1
fi

echo -e '\n> All tests for elasticsearch completed successfully.'
