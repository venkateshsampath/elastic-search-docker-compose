#!/usr/bin/env bash

set -euf
cd scripts

echo -e '\n> waiting for containers to be ready ...'
sleep 120

sh ./elasticsearch-tests.sh
sh ./kibana-tests.sh
sh ./logstash-tests.sh
sh ./filebeat-tests.sh
