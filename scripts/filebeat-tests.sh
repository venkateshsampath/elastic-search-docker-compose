#!/usr/bin/env bash

set -euf -o pipefail
source ../.env

echo -e '\n> Running tests on filebeat ...'

elasticsearch_host_port=$(docker port ${COMPOSE_PROJECT_NAME}_elasticsearch_1 | grep 9200 | cut -f 2 -d ":")

docker exec ${COMPOSE_PROJECT_NAME}_elasticsearch_filebeat_1 filebeat test config c filebeat.yml
docker exec ${COMPOSE_PROJECT_NAME}_logstash_filebeat_1 filebeat test config c filebeat.yml
curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$elasticsearch_host_port/_cat/indices?format=json" | jq '.[].index' | grep filebeat

echo -e '\n> Check document count for filebeat index ...'

filebeat_index=$(curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$elasticsearch_host_port/_cat/indices?format=json" | jq -r '.[].index' | grep filebeat)
curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$elasticsearch_host_port/$filebeat_index/_refresh" > /dev/null
doc_count=$(curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$elasticsearch_host_port/$filebeat_index/_count" | jq -r '.count')

if [[ $doc_count == 1 ]];
then
    echo -e '> Document count is' $doc_count;
else
    echo -e "> Wrong document count."
    exit 1
fi

echo -e '\n > All tests for filebeat completed successfully.'
