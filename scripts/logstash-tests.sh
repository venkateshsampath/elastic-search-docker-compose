#!/usr/bin/env bash

set -euf -o pipefail
source ../.env

echo -e '\n> Running tests on logstash ...'

elasticsearch_host_port=$(docker port ${COMPOSE_PROJECT_NAME}_elasticsearch_1 | grep 9200 | cut -f 2 -d ":")

docker exec ${COMPOSE_PROJECT_NAME}_logstash_1 bin/logstash --config.test_and_exit -f pipeline/

echo -e '\n> Check for logstash index ...'

curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$elasticsearch_host_port/_cat/indices?format=json" | jq '.[].index' | grep logstash

echo -e '\n> Index found'

echo -e '\n> Check document count for logstash index ...'
logstash_index=$(curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$elasticsearch_host_port/_cat/indices?format=json" | jq -r '.[].index' | grep logstash)
curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$elasticsearch_host_port/$logstash_index/_refresh" > /dev/null
doc_count=$(curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$elasticsearch_host_port/$logstash_index/_count" | jq -r '.count')

if [[ $doc_count == 1 ]];
then
    echo -e '> Document count is' $doc_count;
else
    echo -e "> Wrong document count."
    exit 1
fi

echo -e '\n > All tests for logstash completed successfully.'
