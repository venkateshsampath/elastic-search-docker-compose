#!/usr/bin/env bash

set -euf -o pipefail
source ../.env

echo -e '\n> Running tests on kibana ...'

kibana_host_port=$(docker port ${COMPOSE_PROJECT_NAME}_kibana_1 | grep 5601 | cut -f 2 -d ":")

sh ./wait-for-it.sh localhost:$kibana_host_port --timeout=90

resp=$(curl --silent --user elastic:$ELASTIC_USER_PASSWORD -XGET "http://localhost:$kibana_host_port/api/status" | jq '.status.overall.state' )

echo -e '\n> Checking kibana status ...'
if [[ $resp == '"green"' ]];
then
    echo '> Status is green';
else
    echo '> Could not get kibana status,';
fi

echo -e '\n > All tests for kibana completed successfully.'
