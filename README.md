# Elastic Stack for Development Environment #

### Current Project Structure:

```
.
├── Jenkinsfile
├── Makefile
├── README.md
├── docker-compose.yml
├── docker-stack.yml
├── elasticsearch
│   ├── Dockerfile
│   └── config
│       └── elasticsearch.yml
├── filebeat
│   └── Dockerfile
├── kibana
│   ├── Dockerfile
│   └── config
│       └── kibana.yml
└── scripts
    ├── elasticsearch-tests.sh
    ├── kibana-tests.sh
    ├── run-tests.sh
    └── wait-for-it.sh
```

### Docker Compose Usage:
- docker-compose.yml : Used for build and publish
- docker-stack.yml   : Used for Deployment on Dev Environment

